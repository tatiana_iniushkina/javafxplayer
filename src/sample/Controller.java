package sample;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;

import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class Controller {

    @FXML
    private AnchorPane paneSettings;

    @FXML
    private Pane paneHeader;

    @FXML
    private MaterialDesignIconView btnSettings;

    @FXML
    private MaterialDesignIconView btnCloseSettings;

    @FXML
    private MaterialDesignIconView btnClose;

    private static FadeTransition fadeIn = new FadeTransition();
    private static FadeTransition fadeOut = new FadeTransition();

    @FXML
    private void handleButtonAction(MouseEvent event) {
        if (event.getSource() == btnClose) {
            System.exit(0);
        }
        if (event.getSource() == btnSettings) {
//            paneSettings.setVisible(true);
            showTransition(paneSettings);
        }
        if (event.getSource() == btnCloseSettings) {
//            paneSettings.setVisible(false);
            hideTransition(paneSettings);
        }
        if (event.getSource()== paneHeader){
            paneHeader.setVisible(true);
        }

    }

    private void hideTransition(AnchorPane anchorPane) {
        fadeOut.setNode(anchorPane);
        fadeOut.setDuration(Duration.millis(1000));
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);
        anchorPane.setVisible(false);
        fadeOut.play();
    }

    private void showTransition(AnchorPane anchorPane) {
        fadeIn.setNode(anchorPane);
        fadeIn.setDuration(Duration.millis(1000));
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        anchorPane.setVisible(true);
        fadeIn.play();
    }

    @FXML
    void initialize() {

    }
}
